class  Api::V1::TodosController < ApplicationController
  before_action :set_todo, only: [:show, :update, :destroy]

  # GET /todos
  def index
    @todos = Todo.all

    render json: @todos
  end

  # GET /todos/1
  def show
    # puts @todo.expired - Time.now
    # puts  @todo.expired
    # puts Time.now.utc
    # TodoWorker.perform_async(@todo.id)
    render json: @todo
  end

  # POST /todos
  def create
    puts Time.now
    @todo = Todo.new(todo_params)

    if @todo.save
      TodoWorker.perform_in(@todo.expired - Time.now.utc ,@todo.id)
      render json: @todo, status: :created
    else
      render json: @todo.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /todos/1
  def update
    if @todo.update(todo_params)
      render json: @todo
    else
      render json: @todo.errors, status: :unprocessable_entity
    end
  end

  # DELETE /todos/1
  def destroy
    @todo.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_todo
      @todo = Todo.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def todo_params
      params.require(:todo).permit(:title, :expired, :finish)
    end
end
