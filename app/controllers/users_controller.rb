class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  def show
    WorkerJob @user
    render json: @user
  end

  # POST /users
  def create
    # Create the user from params
    @user = User.new(user_params)
    UserNotifierMailer.send_signup_email(user_params).deliver
    if @user.save
      # Deliver the signup email
      render json: @user
    else
      render :action => 'new'
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:name, :email, :login)
    end
end
