class TodoMailer < ApplicationMailer

  default :from => 'any_from_address@example.com'

  # send a signup email to the user, pass in the user object that   contains the user's email address
  def send_todo_mailer(todo)
    @todo = todo
    puts @todo
    mail( :to => "#{@todo.title}@example.com",
    :subject => "Todo Completed #{@todo.title}" )
  end
end
