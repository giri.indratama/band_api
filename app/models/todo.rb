class Todo < ApplicationRecord
  validates :title, presence: true
  validates :expired, presence: true
end
