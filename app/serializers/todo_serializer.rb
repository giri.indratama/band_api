class TodoSerializer < ActiveModel::Serializer
  attributes :id, :title, :finish, :expired
end
