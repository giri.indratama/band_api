class WorkerSerializer < ActiveModel::Serializer
  attributes :id, :name, :start, :end
end
