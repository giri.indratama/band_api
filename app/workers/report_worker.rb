class ReportWorker
  include Sidekiq::Worker

  def perform(start_date, end_date, name_worker)
    # Do something
    puts "Sidekiq worker #{name_worker} a report #{start_date} to #{end_date}"
  end
end
