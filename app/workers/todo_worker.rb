class TodoWorker
  include Sidekiq::Worker

  def perform(id)
    @todo = Todo.find(id)
    @todo.attributes = { finish: true }
    if @todo.save
      TodoMailer.send_todo_mailer(@todo).deliver
      puts json: {status: 'SUCCESS', massage: 'Update todo', data: todo}, status: :ok
    else
      puts json: {status: 'ERROR', massage: 'todo not update', data: @todo.errors}, status: :unprocessable_prosses
    end
  end
end
