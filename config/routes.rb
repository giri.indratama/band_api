Rails.application.routes.draw do
  resources :workers
  resources :products

  require 'sidekiq/web'
  mount Sidekiq::Web => "/sidekiq"

  resources :users
  namespace :api do
    namespace :v1 do
      resources :todos  
      resources :bands do
        resources :members
      end
    end

    namespace :v2 do  
      resources :bands do
        resources :members
      end
    end
  end
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
