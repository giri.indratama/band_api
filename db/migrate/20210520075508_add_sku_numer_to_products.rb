class AddSkuNumerToProducts < ActiveRecord::Migration[6.1]
  def change
    add_column :products, :sku_number, :string
  end
end
